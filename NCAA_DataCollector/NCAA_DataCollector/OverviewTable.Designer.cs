﻿namespace NCAA_DataCollector
{
	partial class OverviewTable
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.c_teamName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.c_homeMargin = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.c_awayMargin = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.c_homeHandycap = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.c_awayHandycap = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.AllowUserToOrderColumns = true;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.c_teamName,
            this.c_homeMargin,
            this.c_awayMargin,
            this.c_homeHandycap,
            this.c_awayHandycap});
			this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView1.Location = new System.Drawing.Point(0, 0);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.Size = new System.Drawing.Size(531, 459);
			this.dataGridView1.TabIndex = 0;
			this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
			this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
			// 
			// c_teamName
			// 
			this.c_teamName.HeaderText = "TeamName";
			this.c_teamName.Name = "c_teamName";
			this.c_teamName.ReadOnly = true;
			// 
			// c_homeMargin
			// 
			this.c_homeMargin.HeaderText = "HomeMargin";
			this.c_homeMargin.Name = "c_homeMargin";
			this.c_homeMargin.ReadOnly = true;
			// 
			// c_awayMargin
			// 
			this.c_awayMargin.HeaderText = "AwayMargin";
			this.c_awayMargin.Name = "c_awayMargin";
			this.c_awayMargin.ReadOnly = true;
			// 
			// c_homeHandycap
			// 
			this.c_homeHandycap.HeaderText = "HomeHandycap";
			this.c_homeHandycap.Name = "c_homeHandycap";
			this.c_homeHandycap.ReadOnly = true;
			// 
			// c_awayHandycap
			// 
			this.c_awayHandycap.HeaderText = "AwayHandycap";
			this.c_awayHandycap.Name = "c_awayHandycap";
			this.c_awayHandycap.ReadOnly = true;
			// 
			// OverviewTable
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(531, 459);
			this.Controls.Add(this.dataGridView1);
			this.Name = "OverviewTable";
			this.Text = "OverviewTable";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OverviewTable_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.DataGridViewTextBoxColumn c_teamName;
		private System.Windows.Forms.DataGridViewTextBoxColumn c_homeMargin;
		private System.Windows.Forms.DataGridViewTextBoxColumn c_awayMargin;
		private System.Windows.Forms.DataGridViewTextBoxColumn c_homeHandycap;
		private System.Windows.Forms.DataGridViewTextBoxColumn c_awayHandycap;
	}
}