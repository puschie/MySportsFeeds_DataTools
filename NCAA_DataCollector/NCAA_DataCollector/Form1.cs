﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace NCAA_DataCollector
{
	public partial class Form1 : Form
	{
		private List<Game>              GameList;
		private List<string>			TeamList;
		private List<TeamMargin>        MarginList;

		private HtmlAgilityPack.HtmlWeb	Web;
		private OverviewTable           MarginOverview;

		private const string            BaseURL = "http://www.ncaa.com/scoreboard/basketball-men/";

		private const string            MarginDataFileName = "MarginData.mdc";

		[Serializable]
		private struct Game
		{
			public int HomeTeamID;
			public int AwayTeamID;
			public int AwayScore;
			public int HomeScore;
			public long DateTicks;
		}

		[Serializable]
		public class TeamMargin
		{
			public int HomeMatchCounter;
			public int AwayMatchCounter;
			public float HomeMatchMarginSum;
			public float AwayMatchMarginSum;
			public float HomeHandycap;
			public float AwayHandycap;
		}

		public Form1()
		{
			InitializeComponent();
			dp_start.MaxDate = DateTime.Today;
			dp_end.MaxDate = DateTime.Today;
			cb_division.SelectedIndex = 0;
			MarginOverview = new OverviewTable( this );

			if( File.Exists( MarginDataFileName ) )
				LoadMarginData( MarginDataFileName );
		}

		public List<TeamMargin> GetMarginList()
		{
			return MarginList;
		}

		public List<string> GetTeamList()
		{
			return TeamList;
		}

		public void SetHandycap( bool Home, float Handycap, int TeamIndex )
		{
			if( Home )
			{
				MarginList[TeamIndex].HomeHandycap = Handycap;
			}
			else
			{
				MarginList[TeamIndex].AwayHandycap = Handycap;
			}
		}

		private bool AskForOverrideData()
		{
			return ( MessageBox.Show( "This action will override the old data", "Fetching new data", MessageBoxButtons.YesNo ) == DialogResult.Yes );
		}

		private void SetControlEnable( bool Value )
		{
			dp_start.Enabled = Value;
			dp_end.Enabled = Value;
			cb_division.Enabled = Value;
			bt_GetWebData.Enabled = Value;
			bt_GetFileData.Enabled = Value;
			bt_SaveFiledata.Enabled = Value;
			bt_table.Enabled = Value;
			//bt_showMatcher.Enabled = Value;
		}

		private void AnalyseWebsite( DateTime date, int Division )
		{
			string DivisionPart = "d" + ( Division + 1 ).ToString() + "/";
			string URL = BaseURL + DivisionPart + date.ToString( "yyyy'/'MM'/'dd" );

			HtmlAgilityPack.HtmlNode ContainerNode = Web.Load( URL ).GetElementbyId( "scoreboard" );
			if( ContainerNode == null ) // break on invalid page
				return;

			ContainerNode = ContainerNode.SelectSingleNode( "//section[@class='day']" );
			HtmlAgilityPack.HtmlNodeCollection GamesNodeCollection = ContainerNode.SelectNodes("//section[contains(@id,'gamecenter-/game/basketball-men')]");

			foreach( HtmlAgilityPack.HtmlNode Node in GamesNodeCollection )
			{
				// 3 ( <div class="day"> -> 5 ( <div class="linescore"> ) -> 1 ( <table class="linescore"> )
				HtmlAgilityPack.HtmlNode TableNode = Node.ChildNodes[3].ChildNodes[5].ChildNodes[1];
				
				HtmlAgilityPack.HtmlNode AwayNode = TableNode.ChildNodes[1]; // Away <tr>
				HtmlAgilityPack.HtmlNode HomeNode = TableNode.ChildNodes[2]; // Home <tr>

				// 0 ( <td class"..."> ) -> 1 ( <div class="team"> ) -> 0 ( <a title="..."> )
				int TeamInfoNodeCount = AwayNode.ChildNodes[0].ChildNodes.Count - 1;
				string AwayTeamName = AwayNode.ChildNodes[0].ChildNodes[TeamInfoNodeCount].ChildNodes[0].InnerText;
				string HomeTeamName = HomeNode.ChildNodes[0].ChildNodes[TeamInfoNodeCount].ChildNodes[0].InnerText;

				if( AwayNode.ChildNodes.Count < 5 ) // skip if no quarter-score exist
					continue;

				// 1 ( <td>FIRST-HALF-SCORE</td> )
				int AwayScoreFirstHalf = Int32.Parse( AwayNode.ChildNodes[1].InnerText );
				int HomeScoreFirstHalf = Int32.Parse( HomeNode.ChildNodes[1].InnerText );

				Game AnalysedGame = new Game();
				AnalysedGame.AwayTeamID = GetTeamID( AwayTeamName );
				AnalysedGame.HomeTeamID = GetTeamID( HomeTeamName );
				AnalysedGame.AwayScore = AwayScoreFirstHalf;
				AnalysedGame.HomeScore = HomeScoreFirstHalf;
				AnalysedGame.DateTicks = date.Ticks;

				GameList.Add( AnalysedGame );
			}
		}

		private int GetTeamID( string TeamName )
		{
			int Index = TeamList.IndexOf( TeamName );
			if( Index == -1 )
			{
				TeamList.Add( TeamName );
				return TeamList.Count - 1;
			}
			return Index;
		}

		private void SaveMarginData( string FileName )
		{
			BinaryFormatter BinSerializer = new BinaryFormatter();
			FileStream BinStream = new FileStream( FileName, FileMode.Create, FileAccess.Write, FileShare.None );
			BinSerializer.Serialize( BinStream, TeamList );
			BinSerializer.Serialize( BinStream, MarginList );
			BinStream.Close();
		}

		private void LoadMarginData( string FileName )
		{
			BinaryFormatter BinSerializer = new BinaryFormatter();
			FileStream BinStream = new FileStream( FileName, FileMode.Open, FileAccess.Read, FileShare.None );
			TeamList = (List<string>)BinSerializer.Deserialize( BinStream );
			MarginList = (List<TeamMargin>)BinSerializer.Deserialize( BinStream );
			BinStream.Close();

			DataLoaded();
		}

		private void CalculateMargins()
		{
			if( MarginList != null )
			{
				MarginList.Clear();
				MarginList.Capacity = TeamList.Count;
			}
			else MarginList = new List<TeamMargin>( TeamList.Count );

			for( int i = 0; i < TeamList.Count; ++i )
			{
				MarginList.Add( new TeamMargin() );
			}
			foreach( Game match in GameList )
			{
				int Value = match.AwayScore - match.HomeScore;
				MarginList[match.AwayTeamID].AwayMatchCounter++;
				MarginList[match.AwayTeamID].AwayMatchMarginSum += Value;
				MarginList[match.HomeTeamID].HomeMatchCounter++;
				MarginList[match.HomeTeamID].HomeMatchMarginSum += -Value;
			}

			SaveMarginData( MarginDataFileName );
			DataLoaded();
		}

		private void DataLoaded()
		{
			MarginOverview.PopulateTable();
			SetControlEnable( true );
		}

		private void bt_GetWebData_Click( object sender, EventArgs e )
		{
			if( bt_table.Enabled && !AskForOverrideData() ) // break if there is data and user says no
				return;

			SetControlEnable( false );

			DateTime TimeDate = dp_start.Value.Date;
			DateTime FinalDate = dp_end.Value.Date;
			int Division = cb_division.SelectedIndex;

			if( GameList == null )
				GameList = new List<Game>();
			else
				GameList.Clear();

			if( TeamList == null )
				TeamList = new List<string>();
			else
				TeamList.Clear();

			if( Web == null )
				Web = new HtmlAgilityPack.HtmlWeb();

			do
			{
				Text = TimeDate.ToString( "yyyy'/'MM'/'dd" );
				AnalyseWebsite( TimeDate, Division );
				TimeDate = TimeDate.AddDays( 1 );
			} while( TimeDate.Ticks != FinalDate.Ticks );

			CalculateMargins();
		}

		private void bt_table_Click( object sender, EventArgs e )
		{
			MarginOverview.Show();
		}

		private void bt_GetFileData_Click( object sender, EventArgs e )
		{
			if( bt_table.Enabled && !AskForOverrideData() )
				return;

			if( openFileDialog1.ShowDialog() == DialogResult.OK )
				LoadMarginData( openFileDialog1.FileName );
		}

		private void bt_SaveFiledata_Click( object sender, EventArgs e )
		{
			if( saveFileDialog1.ShowDialog() == DialogResult.OK )
				SaveMarginData( saveFileDialog1.FileName );
		}

		private void Form1_FormClosed( object sender, FormClosedEventArgs e )
		{
			SaveMarginData( MarginDataFileName );
		}
	}
}
