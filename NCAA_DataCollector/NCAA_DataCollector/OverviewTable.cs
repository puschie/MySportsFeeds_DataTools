﻿using System.Collections.Generic;
using System.Windows.Forms;
using static NCAA_DataCollector.Form1;

namespace NCAA_DataCollector
{
	public partial class OverviewTable : Form
	{
		private Form1 OwnerRef;

		public OverviewTable( Form1 Ref )
		{
			InitializeComponent();
			OwnerRef = Ref;
		}

		public void PopulateTable()
		{
			dataGridView1.Rows.Clear();
			for( int i = 0; i < OwnerRef.GetMarginList().Count; ++i )
			{
				TeamMargin Entry = OwnerRef.GetMarginList()[i];
				float HomeMargin = ( Entry.HomeMatchCounter == 0 ) ? ( 0 ) : ( Entry.HomeMatchMarginSum / Entry.HomeMatchCounter );
				float AwayMargin = ( Entry.AwayMatchCounter == 0 ) ? ( 0 ) : ( Entry.AwayMatchMarginSum / Entry.AwayMatchCounter );
				dataGridView1.Rows.Add( OwnerRef.GetTeamList()[i], HomeMargin + Entry.HomeHandycap, AwayMargin + Entry.AwayHandycap, Entry.HomeHandycap, Entry.AwayHandycap );
			}
		}

		private void OverviewTable_FormClosing( object sender, FormClosingEventArgs e )
		{
			if( e.CloseReason == CloseReason.UserClosing )
			{
				e.Cancel = true;
				Hide();
			}
		}

		private void dataGridView1_CellValueChanged( object sender, DataGridViewCellEventArgs e )
		{
			if( !Visible ) return;

			bool Home = ( e.ColumnIndex == 3 );
			float Value = float.Parse( (string)dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value );
			OwnerRef.SetHandycap( Home, Value, e.RowIndex );
		}

		private void ValueKeyPress( object sender, KeyPressEventArgs e )
		{
			if( !char.IsControl( e.KeyChar ) && !char.IsDigit( e.KeyChar ) && e.KeyChar != ',' )
			{
				e.Handled = true;
			}
		}

		private void dataGridView1_EditingControlShowing( object sender, DataGridViewEditingControlShowingEventArgs e )
		{
			e.Control.KeyPress += new KeyPressEventHandler( ValueKeyPress );
		}
	}
}
