﻿namespace NCAA_DataCollector
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.bt_GetWebData = new System.Windows.Forms.Button();
			this.dp_start = new System.Windows.Forms.DateTimePicker();
			this.dp_end = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.cb_division = new System.Windows.Forms.ComboBox();
			this.bt_table = new System.Windows.Forms.Button();
			this.bt_showMatcher = new System.Windows.Forms.Button();
			this.bt_GetFileData = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.bt_SaveFiledata = new System.Windows.Forms.Button();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.SuspendLayout();
			// 
			// bt_GetWebData
			// 
			this.bt_GetWebData.Location = new System.Drawing.Point(79, 51);
			this.bt_GetWebData.Name = "bt_GetWebData";
			this.bt_GetWebData.Size = new System.Drawing.Size(120, 23);
			this.bt_GetWebData.TabIndex = 0;
			this.bt_GetWebData.Text = "Load Webdata";
			this.bt_GetWebData.UseVisualStyleBackColor = true;
			this.bt_GetWebData.Click += new System.EventHandler(this.bt_GetWebData_Click);
			// 
			// dp_start
			// 
			this.dp_start.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dp_start.Location = new System.Drawing.Point(12, 25);
			this.dp_start.MaxDate = new System.DateTime(2017, 2, 2, 0, 0, 0, 0);
			this.dp_start.MinDate = new System.DateTime(2016, 11, 11, 0, 0, 0, 0);
			this.dp_start.Name = "dp_start";
			this.dp_start.Size = new System.Drawing.Size(81, 20);
			this.dp_start.TabIndex = 2;
			this.dp_start.Value = new System.DateTime(2016, 11, 11, 0, 0, 0, 0);
			// 
			// dp_end
			// 
			this.dp_end.CustomFormat = "";
			this.dp_end.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dp_end.Location = new System.Drawing.Point(114, 25);
			this.dp_end.MaxDate = new System.DateTime(2017, 2, 2, 0, 0, 0, 0);
			this.dp_end.MinDate = new System.DateTime(2016, 11, 11, 0, 0, 0, 0);
			this.dp_end.Name = "dp_end";
			this.dp_end.Size = new System.Drawing.Size(87, 20);
			this.dp_end.TabIndex = 3;
			this.dp_end.Value = new System.DateTime(2017, 2, 2, 0, 0, 0, 0);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(23, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(55, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Start Date";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(127, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(52, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "End Date";
			// 
			// cb_division
			// 
			this.cb_division.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cb_division.FormattingEnabled = true;
			this.cb_division.Items.AddRange(new object[] {
            "D1",
            "D2",
            "D3"});
			this.cb_division.Location = new System.Drawing.Point(12, 52);
			this.cb_division.Name = "cb_division";
			this.cb_division.Size = new System.Drawing.Size(61, 21);
			this.cb_division.TabIndex = 6;
			// 
			// bt_table
			// 
			this.bt_table.Enabled = false;
			this.bt_table.Location = new System.Drawing.Point(110, 109);
			this.bt_table.Name = "bt_table";
			this.bt_table.Size = new System.Drawing.Size(92, 23);
			this.bt_table.TabIndex = 7;
			this.bt_table.Text = "Show Table";
			this.bt_table.UseVisualStyleBackColor = true;
			this.bt_table.Click += new System.EventHandler(this.bt_table_Click);
			// 
			// bt_showMatcher
			// 
			this.bt_showMatcher.Enabled = false;
			this.bt_showMatcher.Location = new System.Drawing.Point(12, 109);
			this.bt_showMatcher.Name = "bt_showMatcher";
			this.bt_showMatcher.Size = new System.Drawing.Size(92, 23);
			this.bt_showMatcher.TabIndex = 8;
			this.bt_showMatcher.Text = "Show Matcher";
			this.bt_showMatcher.UseVisualStyleBackColor = true;
			// 
			// bt_GetFileData
			// 
			this.bt_GetFileData.Location = new System.Drawing.Point(110, 80);
			this.bt_GetFileData.Name = "bt_GetFileData";
			this.bt_GetFileData.Size = new System.Drawing.Size(89, 23);
			this.bt_GetFileData.TabIndex = 9;
			this.bt_GetFileData.Text = "Load Filedata";
			this.bt_GetFileData.UseVisualStyleBackColor = true;
			this.bt_GetFileData.Click += new System.EventHandler(this.bt_GetFileData_Click);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.DefaultExt = "mdc";
			this.openFileDialog1.FileName = "MarginData";
			this.openFileDialog1.Filter = "Margin Data Collection (*.mdc)|*.mdc";
			// 
			// bt_SaveFiledata
			// 
			this.bt_SaveFiledata.Enabled = false;
			this.bt_SaveFiledata.Location = new System.Drawing.Point(12, 80);
			this.bt_SaveFiledata.Name = "bt_SaveFiledata";
			this.bt_SaveFiledata.Size = new System.Drawing.Size(92, 23);
			this.bt_SaveFiledata.TabIndex = 10;
			this.bt_SaveFiledata.Text = "Save Filedata";
			this.bt_SaveFiledata.UseVisualStyleBackColor = true;
			this.bt_SaveFiledata.Click += new System.EventHandler(this.bt_SaveFiledata_Click);
			// 
			// saveFileDialog1
			// 
			this.saveFileDialog1.DefaultExt = "mdc";
			this.saveFileDialog1.FileName = "MarginData";
			this.saveFileDialog1.Filter = "Margin Data Collection (*.mdc)|*.mdc";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(211, 141);
			this.Controls.Add(this.bt_SaveFiledata);
			this.Controls.Add(this.bt_GetFileData);
			this.Controls.Add(this.bt_showMatcher);
			this.Controls.Add(this.bt_table);
			this.Controls.Add(this.cb_division);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.dp_end);
			this.Controls.Add(this.dp_start);
			this.Controls.Add(this.bt_GetWebData);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button bt_GetWebData;
		private System.Windows.Forms.DateTimePicker dp_start;
		private System.Windows.Forms.DateTimePicker dp_end;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox cb_division;
		private System.Windows.Forms.Button bt_table;
		private System.Windows.Forms.Button bt_showMatcher;
		private System.Windows.Forms.Button bt_GetFileData;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button bt_SaveFiledata;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
	}
}

