﻿namespace BetCalculator
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.bt_testAll = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.awaySelection = new System.Windows.Forms.ComboBox();
			this.homeSelection = new System.Windows.Forms.ComboBox();
			this.bt_testCheck = new System.Windows.Forms.Button();
			this.formularField = new System.Windows.Forms.TextBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.teamValuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.playerValuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.outputText = new System.Windows.Forms.RichTextBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
			this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
			this.splitContainer1.Panel1.Controls.Add(this.formularField);
			this.splitContainer1.Panel1.Controls.Add(this.menuStrip1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.outputText);
			this.splitContainer1.Size = new System.Drawing.Size(544, 394);
			this.splitContainer1.SplitterDistance = 193;
			this.splitContainer1.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Right;
			this.groupBox2.Location = new System.Drawing.Point(281, 28);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(263, 105);
			this.groupBox2.TabIndex = 4;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Optimize";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.bt_testAll);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.awaySelection);
			this.groupBox1.Controls.Add(this.homeSelection);
			this.groupBox1.Controls.Add(this.bt_testCheck);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
			this.groupBox1.Location = new System.Drawing.Point(0, 28);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(281, 105);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Testing";
			// 
			// bt_testAll
			// 
			this.bt_testAll.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.bt_testAll.Location = new System.Drawing.Point(12, 76);
			this.bt_testAll.Name = "bt_testAll";
			this.bt_testAll.Size = new System.Drawing.Size(75, 23);
			this.bt_testAll.TabIndex = 5;
			this.bt_testAll.Text = "Test All";
			this.toolTip1.SetToolTip(this.bt_testAll, "Check formula for errors");
			this.bt_testAll.UseVisualStyleBackColor = true;
			this.bt_testAll.Click += new System.EventHandler(this.bt_testAll_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(188, 20);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(60, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "AwayTeam";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(34, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(62, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "HomeTeam";
			// 
			// awaySelection
			// 
			this.awaySelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.awaySelection.FormattingEnabled = true;
			this.awaySelection.Location = new System.Drawing.Point(166, 36);
			this.awaySelection.Name = "awaySelection";
			this.awaySelection.Size = new System.Drawing.Size(109, 21);
			this.awaySelection.TabIndex = 2;
			// 
			// homeSelection
			// 
			this.homeSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.homeSelection.FormattingEnabled = true;
			this.homeSelection.Location = new System.Drawing.Point(12, 36);
			this.homeSelection.Name = "homeSelection";
			this.homeSelection.Size = new System.Drawing.Size(109, 21);
			this.homeSelection.TabIndex = 1;
			// 
			// bt_testCheck
			// 
			this.bt_testCheck.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.bt_testCheck.Location = new System.Drawing.Point(200, 76);
			this.bt_testCheck.Name = "bt_testCheck";
			this.bt_testCheck.Size = new System.Drawing.Size(75, 23);
			this.bt_testCheck.TabIndex = 0;
			this.bt_testCheck.Text = "Check";
			this.toolTip1.SetToolTip(this.bt_testCheck, "Check formula for errors");
			this.bt_testCheck.UseVisualStyleBackColor = true;
			this.bt_testCheck.Click += new System.EventHandler(this.bt_testCheck_Click);
			// 
			// formularField
			// 
			this.formularField.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.formularField.Location = new System.Drawing.Point(0, 133);
			this.formularField.Multiline = true;
			this.formularField.Name = "formularField";
			this.formularField.Size = new System.Drawing.Size(544, 60);
			this.formularField.TabIndex = 1;
			this.formularField.Text = "0.96 * ( {LastGames(0).FieldGoalAttempts} + {LastGames(0).Turnovers} + 0.44 * {La" +
    "stGames(0).FreeThrowAttempts} - {LastGames(0).OffensiveRebounds} )\r\n";
			this.formularField.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.toolTip1.SetToolTip(this.formularField, "Formula field");
			// 
			// menuStrip1
			// 
			this.menuStrip1.ImageScalingSize = new System.Drawing.Size(19, 19);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configToolStripMenuItem,
            this.infoToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(544, 28);
			this.menuStrip1.TabIndex = 2;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// configToolStripMenuItem
			// 
			this.configToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.teamValuesToolStripMenuItem,
            this.playerValuesToolStripMenuItem});
			this.configToolStripMenuItem.Name = "configToolStripMenuItem";
			this.configToolStripMenuItem.Size = new System.Drawing.Size(65, 24);
			this.configToolStripMenuItem.Text = "Config";
			// 
			// teamValuesToolStripMenuItem
			// 
			this.teamValuesToolStripMenuItem.Name = "teamValuesToolStripMenuItem";
			this.teamValuesToolStripMenuItem.Size = new System.Drawing.Size(169, 26);
			this.teamValuesToolStripMenuItem.Text = "Team Values";
			this.teamValuesToolStripMenuItem.Click += new System.EventHandler(this.teamValuesToolStripMenuItem_Click);
			// 
			// playerValuesToolStripMenuItem
			// 
			this.playerValuesToolStripMenuItem.Name = "playerValuesToolStripMenuItem";
			this.playerValuesToolStripMenuItem.Size = new System.Drawing.Size(169, 26);
			this.playerValuesToolStripMenuItem.Text = "Player Values";
			this.playerValuesToolStripMenuItem.Click += new System.EventHandler(this.playerValuesToolStripMenuItem_Click);
			// 
			// infoToolStripMenuItem
			// 
			this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
			this.infoToolStripMenuItem.Size = new System.Drawing.Size(47, 24);
			this.infoToolStripMenuItem.Text = "Info";
			this.infoToolStripMenuItem.Click += new System.EventHandler(this.infoToolStripMenuItem_Click);
			// 
			// outputText
			// 
			this.outputText.Dock = System.Windows.Forms.DockStyle.Fill;
			this.outputText.Location = new System.Drawing.Point(0, 0);
			this.outputText.Name = "outputText";
			this.outputText.ReadOnly = true;
			this.outputText.Size = new System.Drawing.Size(544, 197);
			this.outputText.TabIndex = 0;
			this.outputText.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(544, 394);
			this.Controls.Add(this.splitContainer1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel1.PerformLayout();
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.RichTextBox outputText;
		private System.Windows.Forms.TextBox formularField;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button bt_testCheck;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem teamValuesToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem playerValuesToolStripMenuItem;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox awaySelection;
		private System.Windows.Forms.ComboBox homeSelection;
		private System.Windows.Forms.Button bt_testAll;
		private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
	}
}

