﻿using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using DataClasses;
using System.IO;
using System;
using System.Threading.Tasks;
using System.Threading;

namespace BetCalculator
{
	public partial class Form1 : Form
	{
		private List<Player>	PlayerList;
		private List<Team>      TeamList;
		private List<GamePair>  GameList = new List<GamePair>();
		private List<Play>      PlayList = new List<Play>();
		private ValueTable      ValuesTable;
		private InfoBox         KeywordInfo = new InfoBox();
		private Parser          FormulaParser;

		public Form1()
		{
			InitializeComponent();
			Task.Delay( 200 ).ContinueWith( _ => { InitDataList(); }, CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler.FromCurrentSynchronizationContext() );
		}

		public List<Player> GetPlayerList()
		{
			return PlayerList;
		}

		public List<Team> GetTeamList()
		{
			return TeamList;
		}

		public List<GamePair> GetGameList()
		{
			return GameList;
		}

		public ValueTable GetValueTable()
		{
			return ValuesTable;
		}
		
		// Invoke if required
		public void LogMessage( string Message )
		{
			if( InvokeRequired )
			{
				if( !IsDisposed && Visible )
				{
					Invoke( new Action( () => outputText.AppendText( Message ) ) );
				}
			}
			else
			{
				if( !IsDisposed && Visible )
					 outputText.AppendText( Message );
			}
		}

		// Invoke
		public string GetFormulaText()
		{
			return (string)Invoke( new Func<string>( () => { return formularField.Text; } ) );
		}

		// Invoke
		public Team GetHomeSelection()
		{
			return (Team)Invoke( new Func<Team>( () => { return TeamList[homeSelection.SelectedIndex]; } ) );
		}

		// Invoke
		public Team GetAwaySelection()
		{
			return (Team)Invoke( new Func<Team>( () => { return TeamList[awaySelection.SelectedIndex]; } ) );
		}

		private void InitDataList()
		{
			string Path = Application.StartupPath + "\\Data\\";
			bool DataLoadFailed = false;
			if( Directory.Exists( Path ) )
			{
				BinaryFormatter BinFormatter = new BinaryFormatter();

				if( File.Exists( "Data/" + "PlayerList.bin" ) )
				{
					FileStream BinStream = new FileStream( "Data/" + "PlayerList.bin", FileMode.Open, FileAccess.Read, FileShare.None );
					PlayerList = (List<Player>)BinFormatter.Deserialize( BinStream );
					BinStream.Close();
				}
				else
				{
					outputText.AppendText( "PlayerList.bin not found\n" );
					DataLoadFailed = true;
				}

				if( File.Exists( "Data/" + "TeamList.bin" ) )
				{
					FileStream BinStream = new FileStream( "Data/" + "TeamList.bin", FileMode.Open, FileAccess.Read, FileShare.None );
					TeamList = (List<Team>)BinFormatter.Deserialize( BinStream );
					BinStream.Close();
				}
				else
				{
					outputText.AppendText( "TeamList.bin not found\n" );
					DataLoadFailed = true;
				}

				string[] GameFiles = Directory.GetFiles( "Data/" , "GameList_*.bin" );
				if( GameFiles.Length > 0 )
				{
					List<Game> DayGameList;
					foreach( string GameFile in GameFiles )
					{
						FileStream BinStream = new FileStream( GameFile, FileMode.Open, FileAccess.Read, FileShare.None );
						DayGameList = (List<Game>)BinFormatter.Deserialize( BinStream );
						BinStream.Close();
						foreach( Game Entry in DayGameList )
						{
							GameList.Add( new GamePair( Entry ) );
						}
					}
				}
				else
				{
					outputText.AppendText( "No GameList_*.bin files found\n" );
					DataLoadFailed = true;
				}

				string[] PlayFiles = Directory.GetFiles( Path, "DayPlayList_*.bin" );
				if( GameFiles.Length > 0 )
				{
					List<Play> DayPlayList;
					foreach( string PlayFile in PlayFiles )
					{
						FileStream BinStream = new FileStream( PlayFile, FileMode.Open, FileAccess.Read, FileShare.None );
						DayPlayList = (List<Play>)BinFormatter.Deserialize( BinStream );
						BinStream.Close();
						PlayList.AddRange( DayPlayList );
					}
				}
				else
				{
					outputText.AppendText( "No DayPlayList_*.bin files found\n" );
					DataLoadFailed = true;
				}
			}
			else
			{
				Directory.CreateDirectory( Path );
				outputText.AppendText( "Data directory created - pls move all *.bin files into this and restart the application\n" );
				DataLoadFailed = true;
			}
			if( DataLoadFailed )
			{
				if( MessageBox.Show( "Not all data was loaded successfully - you cant use the tool in this state!\nPlease move all missing files to the 'Data' Subdirectory and restart the application", "Failed to load data", MessageBoxButtons.OK ) == DialogResult.OK )
				{
					Application.Exit();
				}
			}
			else
			{
				// Successfully data loaded
				CalculateStats();
				ValuesTable = new ValueTable( this );
				foreach( Team team in TeamList )
				{
					homeSelection.Items.Add( team.Name );
					awaySelection.Items.Add( team.Name );
				}

				FormulaParser = new Parser( this );

				homeSelection.SelectedIndex = 0;
				awaySelection.SelectedIndex = 1;
			}
		}

		private void CalculateStats()
		{
			foreach( GamePair CurrentGame in GameList )
			{
				List<Play> GamePlays = PlayList.FindAll( p => p.GameID == CurrentGame.GameRef.ID );
				foreach( Play CurrentPlay in GamePlays )
				{
					GameStats.PlayStatistics Target = ( CurrentPlay.Target == Play.Targets.HOME ) ? CurrentGame.StatsRef.Home : CurrentGame.StatsRef.Away;

					switch( CurrentPlay.Type )
					{
						case Play.Types.FIELD_GOAL :
							Target.FieldGoals++;
							Target.FieldGoalAttempts++;
							break;
						case Play.Types.FIELD_GOAL_ATTEMPT :
							Target.FieldGoalAttempts++;
							break;
						case Play.Types.FREE_THROW_ATTEMPT :
							Target.FreeThowAttempts++;
							break;
						case Play.Types.TURNOVER :
							Target.Turnovers++;
							break;
						case Play.Types.OFFENSIVE_REBOUND :
							Target.OffensiveRebounds++;
							break;
						case Play.Types.DEFENSIVE_REBOUND :
							Target.DefensiveRebounds++;
							break;
					}
				}
				if( GamePlays.Count > 0 )
				{
					CurrentGame.StatsRef.Possession = CalculatePossession( CurrentGame.StatsRef.Home, CurrentGame.StatsRef.Away );
				}
			}
			// maybe remove for later use
			PlayList = null;
		}

		private float CalculatePossession( GameStats.PlayStatistics Target, GameStats.PlayStatistics Opponent )
		{
			// calculate possession ( http://www.nbastuffer.com/component/option,com_glossary/Itemid,0/catid,41/func,view/term,Possession/ )

			return 0.5f * ( ( Target.FieldGoalAttempts + 0.4f * Target.FreeThowAttempts - 1.07f * ( Target.OffensiveRebounds / ( Target.OffensiveRebounds + Opponent.DefensiveRebounds ) ) * ( Target.FieldGoalAttempts - Target.FieldGoals ) + Target.Turnovers ) + ( Opponent.FieldGoalAttempts + 0.4f * Opponent.FreeThowAttempts - 1.07f * ( Opponent.OffensiveRebounds / ( Opponent.OffensiveRebounds + Target.DefensiveRebounds ) ) * ( Opponent.FieldGoalAttempts - Opponent.FieldGoals ) + Opponent.Turnovers ) );
		}

		private void teamValuesToolStripMenuItem_Click( object sender, EventArgs e )
		{
			ValuesTable.FillTableWithTeams();
			ValuesTable.Show();
		}

		private void playerValuesToolStripMenuItem_Click( object sender, EventArgs e )
		{
			ValuesTable.FillTableWithPlayers();
			ValuesTable.Show();
		}

		private void bt_testCheck_Click( object sender, EventArgs e )
		{
			FormulaParser.StartCalculateSingle();
		}

		private void bt_testAll_Click( object sender, EventArgs e )
		{
			FormulaParser.StartCalculateAll();
		}

		private void infoToolStripMenuItem_Click( object sender, EventArgs e )
		{
			KeywordInfo.Show();
		}

		private void Form1_FormClosing( object sender, FormClosingEventArgs e )
		{
			FormulaParser.AbordThread();
		}
	}

}
