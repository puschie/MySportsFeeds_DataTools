﻿using DataClasses;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace BetCalculator
{
	public partial class ValueTable : Form
	{
		private Dictionary<int, float>	PlayerValueList;
		private Dictionary<int, float>	TeamValueList;
		private Form1                   OwnerRef;

		private const string PlayerListFilename = "PlayerValueList.bin";
		private const string TeamListFilename = "TeamValueList.bin";

		private enum ViewMode { NONE, TEAM_MODE, PLAYER_MODE };

		private ViewMode Mode = ViewMode.NONE;

		public ValueTable( Form1 Ref )
		{
			InitializeComponent();

			OwnerRef = Ref;
			BinaryFormatter BinFormatter = new BinaryFormatter();
			// TODO : Check if PlayerValueList & TeamValueList fits PlayerList & TeamList
 
			if( File.Exists( "Data/" + PlayerListFilename ) )
			{
				FileStream BinStream = new FileStream( "Data/" + PlayerListFilename, FileMode.Open, FileAccess.Read, FileShare.None );
				PlayerValueList = (Dictionary<int, float>)BinFormatter.Deserialize( BinStream );
				BinStream.Close();
			}
			else
			{
				PlayerValueList = new Dictionary<int, float>( OwnerRef.GetPlayerList().Count );
				foreach( Player player in OwnerRef.GetPlayerList() )
				{
					PlayerValueList.Add( player.ID, 0 );
				}
			}

			if( File.Exists( "Data/" + TeamListFilename ) )
			{
				FileStream BinStream = new FileStream( "Data/" + TeamListFilename, FileMode.Open, FileAccess.Read, FileShare.None );
				TeamValueList = (Dictionary<int, float>)BinFormatter.Deserialize( BinStream );
				BinStream.Close();
			}
			else
			{
				TeamValueList = new Dictionary<int, float>( OwnerRef.GetTeamList().Count );
				foreach( Team team in OwnerRef.GetTeamList() )
				{
					TeamValueList.Add( team.ID, 0 );
				}
			}
		}

		public void FillTableWithTeams()
		{
			if( Mode == ViewMode.TEAM_MODE ) return;
			Mode = ViewMode.TEAM_MODE;
			dataGridView1.Rows.Clear();

			for( int i = 0; i < OwnerRef.GetTeamList().Count; ++i )
			{
				dataGridView1.Rows.Add( OwnerRef.GetTeamList()[i].Name, TeamValueList[OwnerRef.GetTeamList()[i].ID] );
			}
		}

		public void FillTableWithPlayers()
		{
			if( Mode == ViewMode.PLAYER_MODE ) return;
			Mode = ViewMode.PLAYER_MODE;
			dataGridView1.Rows.Clear();

			for( int i = 0; i < OwnerRef.GetPlayerList().Count; ++i )
			{
				dataGridView1.Rows.Add( OwnerRef.GetPlayerList()[i].FirstName + " " + OwnerRef.GetPlayerList()[i].LastName, PlayerValueList[OwnerRef.GetPlayerList()[i].ID] );
			}
		}

		public float GetTeamValue( int ID )
		{
			return TeamValueList[ID];
		}

		public float GetPlayerValue( int ID )
		{
			return PlayerValueList[ID];
		}

		private void SaveDataToFile()
		{
			BinaryFormatter BinFormatter = new BinaryFormatter();
			FileStream BinStream = null;
			switch( Mode )
			{
				case ViewMode.PLAYER_MODE :
					BinStream = new FileStream( "Data/" + PlayerListFilename, FileMode.Create, FileAccess.Write, FileShare.None );
					BinFormatter.Serialize( BinStream, PlayerValueList );
					break;

				case ViewMode.TEAM_MODE :
					BinStream = new FileStream( "Data/" + TeamListFilename, FileMode.Create, FileAccess.Write, FileShare.None );
					BinFormatter.Serialize( BinStream, TeamValueList );
					break;
				default :
					return;
			}
			BinStream.Close();
		}

		private void ValueKeyPress( object sender, KeyPressEventArgs e )
		{
			if( !char.IsControl( e.KeyChar ) && !char.IsDigit( e.KeyChar ) && e.KeyChar != ',' )
			{
				e.Handled = true;
			}
		}

		private void dataGridView1_CellValueChanged( object sender, DataGridViewCellEventArgs e )
		{
			if( !Visible ) return;

			float Value = float.Parse( (string)dataGridView1.Rows[e.RowIndex].Cells[1].Value );
			switch( Mode )
			{

				case ViewMode.PLAYER_MODE:
					PlayerValueList[OwnerRef.GetPlayerList()[e.RowIndex].ID] = Value;
					break;

				case ViewMode.TEAM_MODE:
					TeamValueList[OwnerRef.GetTeamList()[e.RowIndex].ID] = Value;
					break;
				default:
					return;
			}
			SaveDataToFile();
		}

		private void dataGridView1_EditingControlShowing( object sender, DataGridViewEditingControlShowingEventArgs e )
		{
			e.Control.KeyPress += new KeyPressEventHandler( ValueKeyPress );
		}

		private void ValueTable_FormClosing( object sender, FormClosingEventArgs e )
		{
			if( e.CloseReason == CloseReason.UserClosing )
			{
				e.Cancel = true;
				Hide();
			}
		}
	}
}
