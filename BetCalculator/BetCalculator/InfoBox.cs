﻿using System.Drawing;
using System.Windows.Forms;

namespace BetCalculator
{
	public partial class InfoBox : Form
	{
		private const string RESULT_BOOL = "Bool ( 1 or 0 )";
		private const string RESULT_FLOAT = "Float ( X.X )";
		private const string RESULT_INT = "Int ( X )";
		private const string RESULT_GAME = "Game";

		public InfoBox()
		{
			InitializeComponent();
			AddKeywordLine( "IsHome", RESULT_BOOL );
			AddKeywordLine( "IsAway", RESULT_BOOL );
			AddKeywordLine( "TableTeamValue", RESULT_FLOAT );
			AddKeywordLine( "TableTeamValueOpponent", RESULT_FLOAT );
			AddKeywordLine( "SumPlayersValue", RESULT_FLOAT );
			AddKeywordLine( "SumPlayersValueOpponent", RESULT_FLOAT );
			AddKeywordLine( "HasRookie", RESULT_BOOL );
			AddKeywordLine( "HasRookieOpponent", RESULT_BOOL );
			AddKeywordLine( "CountRookies", RESULT_INT );
			AddKeywordLine( "CountRookiesOpponent", RESULT_INT );
			AddKeywordLine( "LastGames( X )", RESULT_GAME );
			AddKeywordLine( "LastGamesOpponent( X )", RESULT_GAME );
			AddKeywordLine( "LastGamesCount", RESULT_INT );
			AddKeywordLine( "LastGamesCountOpponent", RESULT_INT );
			AddKeywordLine( "Game.Victory", RESULT_BOOL );
			AddKeywordLine( "Game.VictoryOpponent", RESULT_BOOL );
			AddKeywordLine( "Game.QuaterVictory( X )", RESULT_BOOL );
			AddKeywordLine( "Game.QuaterVictoryOpponent( X )", RESULT_BOOL );
			AddKeywordLine( "Game.FieldGoalAttempts", RESULT_INT );
			AddKeywordLine( "Game.FieldGoalAttemptsOpponent", RESULT_INT );
			AddKeywordLine( "Game.Turnovers", RESULT_INT );
			AddKeywordLine( "Game.TurnoversOpponent", RESULT_INT );
			AddKeywordLine( "Game.FreeThrowAttempts", RESULT_INT );
			AddKeywordLine( "Game.FreeThrowAttemptsOpponent", RESULT_INT );
			AddKeywordLine( "Game.OffensiveRebounds", RESULT_INT );
			AddKeywordLine( "Game.OffensiveReboundsOpponent", RESULT_INT );
			AddKeywordLine( "Game.DefensiveRebounds", RESULT_INT );
			AddKeywordLine( "Game.DefensiveReboundsOpponent", RESULT_INT );
			AddKeywordLine( "Game.Possession", RESULT_FLOAT );
		}

		private void AddKeywordLine( string Keyword, string Info )
		{
			richTextBox1.SelectionFont = new Font( richTextBox1.Font, FontStyle.Bold );
			richTextBox1.AppendText( Keyword );
			richTextBox1.SelectionFont = new Font( richTextBox1.Font, FontStyle.Regular );
			richTextBox1.AppendText( " => " + Info + '\n' );
		}

		private void InfoBox_FormClosing( object sender, FormClosingEventArgs e )
		{
			if( e.CloseReason == CloseReason.UserClosing )
			{
				e.Cancel = true;
				Hide();
			}
		}
	}
}
