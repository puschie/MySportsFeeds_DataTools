﻿using DataClasses;

namespace BetCalculator
{
	public class GameStats
	{
		public PlayStatistics Home = new PlayStatistics();
		public PlayStatistics Away = new PlayStatistics();
		public float Possession;

		public class PlayStatistics
		{
			public int FieldGoals;
			public int FieldGoalAttempts;
			public int FreeThowAttempts;
			public int Turnovers;
			public int OffensiveRebounds;
			public int DefensiveRebounds;
		}
	}

	public struct GamePair
	{
		public Game GameRef;
		public GameStats StatsRef;

		public GamePair( Game GRef )
		{
			GameRef = GRef;
			StatsRef = new GameStats();
		}
	}

}
