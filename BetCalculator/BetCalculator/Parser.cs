﻿using DataClasses;
using org.mariuszgromada.math.mxparser;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;

namespace BetCalculator
{
	class Parser
	{
		private Form1 OwnerRef;

		private ParseConfig     CurrentConfig;
		private bool            RunningCalculation = false;
		private Thread          WorkingThread = null;

		private List<Player>    HomeTeamPlayers = new List<Player>();
		private List<Player>    AwayTeamPlayers = new List<Player>();
		private List<GamePair>  HomeTeamGames = new List<GamePair>();
		private List<GamePair>  AwayTeamGames = new List<GamePair>();

		private Expression		MathParser = new Expression();
		private NumberFormatInfo NumberInfo = new NumberFormatInfo();

		private event Action<ResultCodes> ThreadFinished;
		public event Action<ResultCodes> ParsingCompleted;

		private enum ParsePassthrough
		{
			HomeTeam,
			AwayTeam
		}

		private enum LogLevels
		{
			NONE,
			MINIMAL,
			MAXIMALE
		}

		private struct ParseConfig
		{
			public Team TargetTeam;
			public Team OpponentTeam;
			public bool IsHome;
		}

		public enum ResultCodes
		{
			UNKNOWN_ERROR = -1,
			SUCCESS = 0
		}

		public Parser( Form1 Ref )
		{
			OwnerRef = Ref;
			NumberInfo.NumberDecimalSeparator = ".";
			ThreadFinished += RequestCompleted;
		}

		public void StartCalculateAll()
		{
			if( RunningCalculation ) return;
			WorkingThread = new Thread( () => Thread_CalcAll() );
			WorkingThread.Start();
		}

		public void StartCalculateSingle()
		{
			if( RunningCalculation ) return;
			WorkingThread = new Thread( () => Thread_CalcSingle() );
			WorkingThread.Start();
		}

		private void RequestCompleted( ResultCodes Result )
		{
			WorkingThread = null;
			RunningCalculation = false;

			ParsingCompleted?.Invoke( Result );
		}

		private void Thread_CalcAll()
		{
			int SuccessCounter = 0;
			float SuccessRate = 0;
			foreach( GamePair Match in OwnerRef.GetGameList() )
			{
				Team HomeTeam = OwnerRef.GetTeamList().Find( x => x.ID == Match.GameRef.HomeTeamID );
				Team AwayTeam = OwnerRef.GetTeamList().Find( x => x.ID == Match.GameRef.AwayTeamID );
				string[] Formulas = GenerateFormula( HomeTeam, AwayTeam );
				if( Formulas == null )
					continue;
				float HomeResult = (float)CalculateExpression( Formulas[0] );
				float AwayResult = (float)CalculateExpression( Formulas[1] );
				// only use quater [0] and [1]
				int HomeScore = Match.GameRef.QuaterScores[0].HomeScore + Match.GameRef.QuaterScores[1].HomeScore;
				int AwayScore = Match.GameRef.QuaterScores[0].AwayScore + Match.GameRef.QuaterScores[1].AwayScore;

				int GameResult = 0, CalcResult = 0;
				if( HomeScore > AwayScore ) GameResult = 1;
				else if( HomeScore < AwayScore ) GameResult = 2;
				if( HomeResult > AwayResult ) CalcResult = 1;
				else if( HomeResult < AwayResult ) CalcResult = 2;

				if( GameResult == CalcResult ) SuccessCounter++;

				float HomeMatching = ( HomeResult < HomeScore ) ? HomeResult / HomeScore : HomeScore / HomeResult;
				float AwayMatching = ( AwayResult < AwayScore ) ? AwayResult / AwayScore : AwayScore / AwayResult;

				float MatchRate = HomeMatching * 0.5f + AwayMatching * 0.5f;
				SuccessRate += MatchRate;
				OwnerRef.LogMessage( "Testing for Game #" + Match.GameRef.ID + " | " + HomeTeam.Name + " vs " + AwayTeam.Name + '\n' );
				OwnerRef.LogMessage( "Result ( Home : " + HomeResult + " | o. " + HomeScore + " ) ( Away : " + AwayResult + " | o. " + AwayScore + " )\n" );
				OwnerRef.LogMessage( "Matching rate : " + MatchRate * 100 + '\n' );
			}

			OwnerRef.LogMessage( "----------\nTest completed - Game result match : " + SuccessCounter + " of " + OwnerRef.GetGameList().Count +
				" with overall matching rate : " + ( SuccessRate / OwnerRef.GetGameList().Count ) * 100 + "% ( only win/lose " + ( SuccessCounter / (double)OwnerRef.GetGameList().Count ) * 100.0 + "% )\n" );

			ThreadFinished.Invoke( ResultCodes.SUCCESS );
		}

		private void Thread_CalcSingle()
		{

			string[] Formulas = GenerateFormula( OwnerRef.GetHomeSelection(), OwnerRef.GetAwaySelection(), LogLevels.MAXIMALE );
			if( Formulas == null )
				return;

			double HomeResult = CalculateExpression( Formulas[0] );
			double AwayResult = CalculateExpression( Formulas[1] );
			OwnerRef.LogMessage( "Home result : " + HomeResult.ToString() + "\nAway result : " + AwayResult.ToString() + '\n' );

			ThreadFinished.Invoke( ResultCodes.SUCCESS );
		}

		public void AbordThread()
		{
			if( WorkingThread != null )
				WorkingThread.Abort();
		}

		private double CalculateExpression( string MathExpression )
		{
			MathParser.setExpressionString( MathExpression );
			return MathParser.calculate();
		}

		private string GetParsedFormula( List<KeyValuePair<int, int>> KeywordList )
		{
			StringBuilder Result = new StringBuilder( OwnerRef.GetFormulaText() );
			foreach( KeyValuePair<int, int> CustomField in KeywordList )
			{
				int Length = CustomField.Value - CustomField.Key - 1;
				string ParsedKeyword = ParseSingleKeyword( Result.ToString( CustomField.Key + 1, Length ) );
				Result.Remove( CustomField.Key, Length + 2 );
				Result.Insert( CustomField.Key, ParsedKeyword );
			}
			return Result.ToString();
		}

		private string[] GenerateFormula( Team HomeTeam, Team AwayTeam, LogLevels Loglevel = LogLevels.NONE )
		{
			if( HomeTeam == AwayTeam )
			{
				OwnerRef.LogMessage( "You cannot select the same team as home and away\n" );
				return null;
			}

			List<KeyValuePair<int, int>> KeywordList = FindKeywords();

			foreach( int PlayerID in HomeTeam.PlayerIDList )
			{
				HomeTeamPlayers.Add( OwnerRef.GetPlayerList().Find( x => x.ID == PlayerID ) );
			}
			foreach( int PlayerID in AwayTeam.PlayerIDList )
			{
				AwayTeamPlayers.Add( OwnerRef.GetPlayerList().Find( x => x.ID == PlayerID ) );
			}
			// TODO : use last game based on time ( before current game for testing )
			foreach( GamePair game in OwnerRef.GetGameList() )
			{
				if( game.GameRef.HomeTeamID == HomeTeam.ID ||
					game.GameRef.AwayTeamID == HomeTeam.ID )
					HomeTeamGames.Add( game );

				if( HomeTeamGames.Count == 10 )
					break;
			}
			foreach( GamePair game in OwnerRef.GetGameList() )
			{
				if( game.GameRef.HomeTeamID == AwayTeam.ID ||
					game.GameRef.AwayTeamID == AwayTeam.ID )
					AwayTeamGames.Add( game );

				if( AwayTeamGames.Count == 10 )
					break;
			}

			string[] Result = new string[2];

			SetupParseConfig( ParsePassthrough.HomeTeam );
			Result[0] = GetParsedFormula( KeywordList );
			SetupParseConfig( ParsePassthrough.AwayTeam );
			Result[1] = GetParsedFormula( KeywordList );

			if( Loglevel > LogLevels.NONE )
			{
				OwnerRef.LogMessage( "HomeTeam : " + HomeTeam.Name + "\nAwayTeam : " + AwayTeam.Name + '\n' );
				OwnerRef.LogMessage( "HomeTeam formula : " + Result[0] + '\n' );
				OwnerRef.LogMessage( "AwayTeam formula : " + Result[1] + '\n' );
			}

			HomeTeamPlayers.Clear();
			AwayTeamPlayers.Clear();
			HomeTeamGames.Clear();
			AwayTeamGames.Clear();
			return Result;
		}

		private List<KeyValuePair<int, int>> FindKeywords()
		{
			// TODO : add support for subformulas
			//string[] SubFormulas = formularField.Text.Split(';');

			List<KeyValuePair<int, int>> OccurentCustomFields = new List<KeyValuePair<int, int>>();
			int Index = 0;
			while( Index != -1 )
			{
				Index = OwnerRef.GetFormulaText().IndexOf( '{', Index );
				if( Index != -1 )
				{
					int FoundEnd = OwnerRef.GetFormulaText().IndexOf( '}', Index );
					if( FoundEnd == -1 )
					{
						OwnerRef.LogMessage( "Missing '}' to close customBlock | Start '{' at " + Index.ToString() + " \n" );
						break;
					}
					if( FoundEnd - Index < 2 )
					{
						OwnerRef.LogMessage( "Empty Keyword found at " + Index.ToString() + " ( will be ignored )\n" );
					}
					else OccurentCustomFields.Add( new KeyValuePair<int, int>( Index, FoundEnd ) );
					Index = FoundEnd;
				}
			}
			OccurentCustomFields.Reverse();
			return OccurentCustomFields;
		}

		private void SetupParseConfig( ParsePassthrough Stage )
		{
			switch( Stage )
			{
				case ParsePassthrough.HomeTeam:
					CurrentConfig.TargetTeam = OwnerRef.GetHomeSelection();
					CurrentConfig.OpponentTeam = OwnerRef.GetAwaySelection();
					CurrentConfig.IsHome = true;
					break;

				case ParsePassthrough.AwayTeam:
					CurrentConfig.TargetTeam = OwnerRef.GetAwaySelection();
					CurrentConfig.OpponentTeam = OwnerRef.GetHomeSelection();
					CurrentConfig.IsHome = false;
					break;
			}
		}

		// Parse Helper functions
		private bool GetFinal_IsHome( bool Own = true )
		{
			return ( Own ) ? ( CurrentConfig.IsHome ) : ( !CurrentConfig.IsHome );
		}

		private string GetGameResult( Game Target, bool Home, bool Own, int Quater = -1 )
		{
			int HomeScore = ( Quater != -1 ) ? Target.QuaterScores[Quater].HomeScore : 0;
			int AwayScore = ( Quater != -1 ) ? Target.QuaterScores[Quater].AwayScore : 0;

			if( Quater == -1 )
			{
				foreach( scores score in Target.QuaterScores )
				{
					HomeScore += score.HomeScore;
					AwayScore += score.AwayScore;
				}
			}
			if( HomeScore == AwayScore )
				return "0";
			if( HomeScore > AwayScore )
				return ( Home == Own ) ? "1" : "0";
			else
				return ( Home ^ Own ) ? "1" : "0";
		}

		private string KEYWORD_TableTeamValue( bool Own = true )
		{
			return OwnerRef.GetValueTable().GetTeamValue( Own ? CurrentConfig.TargetTeam.ID : CurrentConfig.OpponentTeam.ID ).ToString( NumberInfo );
		}

		private string KEYWORD_SumPlayersValue( bool Own = true )
		{
			float SumValue = 0;
			foreach( int PlayerID in ( Own ? CurrentConfig.TargetTeam.PlayerIDList : CurrentConfig.OpponentTeam.PlayerIDList ) )
			{
				SumValue += OwnerRef.GetValueTable().GetPlayerValue( PlayerID );
			}
			return SumValue.ToString( NumberInfo );
		}

		private string KEYWORD_HasRookie( bool Own = true )
		{
			foreach( Player player in GetFinal_IsHome( Own ) ? HomeTeamPlayers : AwayTeamPlayers )
			{
				if( player.IsRookie )
					return "1";
			}
			return "0";
		}

		private string KEYWORD_CountRookies( bool Own = true )
		{
			int Counter = 0;
			foreach( Player player in GetFinal_IsHome( Own ) ? HomeTeamPlayers : AwayTeamPlayers )
			{
				if( player.IsRookie )
					Counter++;
			}
			return Counter.ToString();
		}

		private string KEYWORD_LastGames( List<int> Parameters, string[] Keywords, bool Own = true )
		{
			if( Keywords.Length == 1 )
			{
				OwnerRef.LogMessage( "Missing Subkeyword after 'LastGames' - 0 will be used\n" );
				return "0";
			}

			if( Parameters[0] == Int32.MaxValue || Parameters[0] < 0 || Parameters[0] >= ( GetFinal_IsHome( Own ) ? HomeTeamGames.Count : AwayTeamGames.Count ) )
			{
				OwnerRef.LogMessage( "Invalid or missing Parameter for 'LastGames' - 0 will be used\n" );
				Parameters[0] = 0;
			}

			GamePair TargetGame = GetFinal_IsHome( Own ) ? HomeTeamGames[Parameters[0]] : AwayTeamGames[Parameters[0]];
			int TeamID = ( Own ? CurrentConfig.TargetTeam.ID : CurrentConfig.OpponentTeam.ID );
			bool Home = TargetGame.GameRef.HomeTeamID == TeamID;
			switch( Keywords[1] )
			{
				case "Victory":
					return KEYWORD_LastGame_Victory( TargetGame.GameRef, Home );
				case "VictoryOpponent":
					return KEYWORD_LastGame_Victory( TargetGame.GameRef, Home, false );
				case "QuaterVictory":
					return KEYWORD_LastGame_QuaterVictory( TargetGame.GameRef, Home, Parameters[1] );
				case "QuaterVictoryOpponent":
					return KEYWORD_LastGame_QuaterVictory( TargetGame.GameRef, Home, Parameters[1], false );
				case "FieldGoalAttempts":
					return KEYWORD_LastGame_StatisticField( TargetGame, Home, Play.Types.FIELD_GOAL_ATTEMPT );
				case "FieldGoalAttemptsOpponent":
					return KEYWORD_LastGame_StatisticField( TargetGame, Home, Play.Types.FIELD_GOAL_ATTEMPT, false );
				case "Turnovers":
					return KEYWORD_LastGame_StatisticField( TargetGame, Home, Play.Types.TURNOVER );
				case "TurnoversOpponent":
					return KEYWORD_LastGame_StatisticField( TargetGame, Home, Play.Types.TURNOVER, false );
				case "FreeThrowAttempts":
					return KEYWORD_LastGame_StatisticField( TargetGame, Home, Play.Types.FREE_THROW_ATTEMPT );
				case "FreeThrowAttemptsOpponent":
					return KEYWORD_LastGame_StatisticField( TargetGame, Home, Play.Types.FREE_THROW_ATTEMPT, false );
				case "OffensiveRebounds":
					return KEYWORD_LastGame_StatisticField( TargetGame, Home, Play.Types.OFFENSIVE_REBOUND );
				case "OffensiveReboundsOpponent":
					return KEYWORD_LastGame_StatisticField( TargetGame, Home, Play.Types.OFFENSIVE_REBOUND, false );
				case "DefensiveRebounds":
					return KEYWORD_LastGame_StatisticField( TargetGame, Home, Play.Types.DEFENSIVE_REBOUND );
				case "DefensiveReboundsOpponent":
					return KEYWORD_LastGame_StatisticField( TargetGame, Home, Play.Types.DEFENSIVE_REBOUND, false );
				case "Possession":
					return TargetGame.StatsRef.Possession.ToString( NumberInfo );
				default:
					OwnerRef.LogMessage( "Unknown Keyword ( on 'LastGames' ) used : " + Keywords[1] + " ( 0 will be used )\n" );
					return "0";
			}
		}

		private string KEYWORD_LastGame_Victory( Game TargetGame, bool Home, bool Own = true )
		{
			return GetGameResult( TargetGame, Home, Own );
		}

		private string KEYWORD_LastGame_QuaterVictory( Game TargetGame, bool Home, int Parameter, bool Own = true )
		{
			if( Parameter == Int32.MaxValue || Parameter < 0 || Parameter > 3 )
			{
				OwnerRef.LogMessage( "Invalid or missing Parameter for 'LastGames.QuaterVictory' - 0 will be used\n" );
				Parameter = 0;
			}
			return GetGameResult( TargetGame, Home, Own, Parameter );
		}

		private string KEYWORD_LastGame_StatisticField( GamePair TargetGame, bool Home, Play.Types Type, bool Own = true )
		{
			GameStats.PlayStatistics Target = ( Home == Own ) ? TargetGame.StatsRef.Home : TargetGame.StatsRef.Away;

			switch( Type )
			{
				case Play.Types.FIELD_GOAL :
					return Target.FieldGoals.ToString();
				case Play.Types.FIELD_GOAL_ATTEMPT:
					return Target.FieldGoalAttempts.ToString();
				case Play.Types.FREE_THROW_ATTEMPT:
					return Target.FreeThowAttempts.ToString();
				case Play.Types.TURNOVER:
					return Target.Turnovers.ToString();
				case Play.Types.OFFENSIVE_REBOUND:
					return Target.OffensiveRebounds.ToString();
				case Play.Types.DEFENSIVE_REBOUND:
					return Target.DefensiveRebounds.ToString();
			}
			return "0";
		}

		// TODO : rewrite code to use mXparser user functions and so on...
		private string ParseSingleKeyword( string KeywordString )
		{
			string[] Keywords = KeywordString.Split('.');
			// Get Keyword Parameter
			List<int> Parameters = new List<int>( Keywords.Length );
			for( int i = 0; i < Keywords.Length; ++i )
			{
				int StartBracket = Keywords[i].IndexOf('(');
				if( StartBracket != -1 )
				{
					int EndBracket = Keywords[i].IndexOf(')');
					if( EndBracket != -1 )
					{
						Parameters.Add( Int32.Parse( Keywords[i].Substring( StartBracket + 1, EndBracket - StartBracket - 1 ) ) );
						Keywords[i] = Keywords[i].Substring( 0, StartBracket );
						continue;
					}
					OwnerRef.LogMessage( "Missing ')' to close Keyword Parameter | Start '(' at " + StartBracket.ToString() + "\n" );
				}
				Parameters.Add( Int32.MaxValue );
			}

			switch( Keywords[0] )
			{
				case "IsHome":
					return CurrentConfig.IsHome ? "1" : "0";
				case "IsAway":
					return CurrentConfig.IsHome ? "0" : "1";
				case "TableTeamValue":
					return KEYWORD_TableTeamValue();
				case "TableTeamValueOpponent":
					return KEYWORD_TableTeamValue( false );
				case "SumPlayersValue":
					return KEYWORD_SumPlayersValue();
				case "SumPlayersValueOpponent":
					return KEYWORD_SumPlayersValue( false );
				case "HasRookie":
					return KEYWORD_HasRookie();
				case "HasRookieOpponent":
					return KEYWORD_HasRookie( false );
				case "CountRookies":
					return KEYWORD_CountRookies();
				case "CountRookiesOpponent":
					return KEYWORD_CountRookies( false );
				case "LastGames":
					return KEYWORD_LastGames( Parameters, Keywords );
				case "LastGamesOpponent":
					return KEYWORD_LastGames( Parameters, Keywords, false );
				case "LastGamesCount":
					return CurrentConfig.IsHome ? HomeTeamGames.Count.ToString() : AwayTeamGames.Count.ToString();
				case "LastGamesCountOpponent":
					return CurrentConfig.IsHome ? AwayTeamGames.Count.ToString() : HomeTeamGames.Count.ToString();
				default:
					OwnerRef.LogMessage( "Unknown Keyword used : " + KeywordString + " ( 0 will be used )\n" );
					return "0";
			}
		}
	}
}
