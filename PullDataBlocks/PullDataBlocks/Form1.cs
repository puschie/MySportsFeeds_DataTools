﻿using DataClasses;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace PullDataBlocks
{
	public partial class Form1 : Form
	{
		APIRequest APIHandle = new APIRequest();
		InfoForm    InfoBox = new InfoForm();

		List<Game>  PlaysGameList;
		List<Team>  PlaysTeamList;
		List<Play>  PlaysList;

		string HomeAbb = null;
		string AwayAbb = null;

		bool FullFetch = false;

		public Form1()
		{
			InitializeComponent();
			seasonDropBox.SelectedIndex = 0;
			pullType.SelectedIndex = 0;
			dateTimePicker1.CustomFormat = "yyyyMMdd";
			APIHandle.RequestResponse += RequestResponse;
		}

		private void RequestResponse( APIRequest.ResultCodes Result )
		{
			if( !IsDisposed && Visible )
			{
				Invoke( new Action( () => RequestFinished( Result ) ) );
			}
		}

		private void RequestFinished( APIRequest.ResultCodes Result )
		{
			if( Result == APIRequest.ResultCodes.NO_ERROR )
			{
				outputText.AppendText( "Request completed - data received\n" );

				if( showJSON.Checked )
				{
					InfoBox.SetText( APIHandle.GetLastResult() );
					InfoBox.Show();
				}

				if( saveData.Checked ) SaveData();
			}
			else
			{
				outputText.AppendText( "Request failed - " + Result.ToString() + "\n" );
				if( (int)Result > 1 ) outputText.AppendText( "Visit 'mysportsfeeds.com -> API -> Response Codes' for more Informations ( Code : " + (int)Result + " )\n" );
				FullFetch = false;
			}

			if( FullFetch )
			{
				// check for fullfetch end
				if( seasonDropBox.SelectedIndex == 0 )
				{
					if( dateTimePicker1.Text == "20160413" ) FullFetch = false;
				}
				else if( seasonDropBox.SelectedIndex == 1 )
				{
					if( dateTimePicker1.Text == "20170412" ) FullFetch = false;
				}
				else if( seasonDropBox.SelectedIndex == 2 )
				{
					if( dateTimePicker1.Text == "20160530" ) FullFetch = false;
				}
				if( !FullFetch ) outputText.AppendText( "'Pull All' completed\n" );
			}

			if( FullFetch )
			{
				dateTimePicker1.Value = dateTimePicker1.Value.AddDays( 1 );
				
				Pull_Click( null, null );
			}
			else if( PlaysGameList != null && PlaysGameList.Count > 0 )
			{
				Pull_Click( null, null );
			}
			else ResetFields();
		}

		private void SaveData()
		{
			if(	APIHandle.GetLastResult() == "{\"scoreboard\":{\"lastUpdatedOn\":null}}" ||
				APIHandle.GetLastResult().Length == 0 )
			{
				outputText.AppendText( "No Data to save\n" );
				return;
			}
			else outputText.AppendText( "Start parse data and save to files\n" );

			JObject RequestResult = JObject.Parse( APIHandle.GetLastResult() );
			IList<JToken> TokenResult;

			switch( pullType.SelectedIndex )
			{
				case 0 : // Player and Teams
					{
						List<Player>[] TeamPlayerList = new List<Player>[1024];
						List<Player> PlayerList = new List<Player>();
						List<Team> TeamList = new List<Team>();

						// TODO : collect players ids for each team - and add to TeamList

						TokenResult = RequestResult["activeplayers"]["playerentry"].Children().ToList();
						foreach( JToken token in TokenResult )
						{
							Player PlayerData = new Player();
							// Player
							// token -> player -> { ... }
							JToken PlayerTokens = token.First.First;
							JToken TeamTokens = token.Last.First;

							PlayerData.ID = PlayerTokens["ID"].Value<int>();
							PlayerData.LastName	= PlayerTokens["LastName"].Value<string>();
							PlayerData.FirstName = PlayerTokens["FirstName"].Value<string>();
							PlayerData.Position	= PlayerTokens["Position"].Value<string>();
							PlayerData.IsRookie = PlayerTokens["IsRookie"].Value<bool>();
							PlayerData.TeamID = TeamTokens["ID"].Value<int>();

							if( PlayerTokens["JerseyNumber"] != null ) PlayerData.JerseyNumber = PlayerTokens["JerseyNumber"].Value<int>();
							if( PlayerTokens["Height"] != null ) PlayerData.Height = PlayerTokens["Height"].Value<string>();
							if( PlayerTokens["Weight"] != null ) PlayerData.Weight = PlayerTokens["Weight"].Value<int>();
							if( PlayerTokens["BirthDate"] != null )	PlayerData.BirthDate = PlayerTokens["BirthDate"].Value<string>();
							if( PlayerTokens["Age"] != null ) PlayerData.Age = PlayerTokens["Age"].Value<int>();
							if( PlayerTokens["BirthCity"] != null ) PlayerData.BirthCity = PlayerTokens["BirthCity"].Value<string>();
							if( PlayerTokens["BirthCountry"] != null ) PlayerData.BirthCountry = PlayerTokens["BirthCountry"].Value<string>();

							PlayerList.Add( PlayerData );

							if( TeamPlayerList[PlayerData.TeamID] == null )
							{
								Team TeamData = new Team();
								TeamPlayerList[PlayerData.TeamID] = new List<Player>();
								
								TeamData.ID				= TeamTokens["ID"].Value<int>();
								TeamData.City			= TeamTokens["City"].Value<string>();
								TeamData.Name			= TeamTokens["Name"].Value<string>();
								TeamData.Abbreviation	= TeamTokens["Abbreviation"].Value<string>();
								TeamList.Add( TeamData );
							}
							TeamPlayerList[PlayerData.TeamID].Add( PlayerData );
						}

						foreach( Team team in TeamList )
						{
							List<Player> TeamPlayers = TeamPlayerList[team.ID];
							team.PlayerIDList = new int[TeamPlayers.Count];
							for( int i = 0; i < TeamPlayers.Count; ++i )
							{
								team.PlayerIDList[i] = TeamPlayers[i].ID;
							}
						}

						BinaryFormatter BinSerializer = new BinaryFormatter();
						FileStream BinStream = new FileStream( "Data/PlayerList.bin", FileMode.Create, FileAccess.Write, FileShare.None );
						BinSerializer.Serialize( BinStream, PlayerList );
						BinStream.Close();
						BinStream = new FileStream( "Data/TeamList.bin", FileMode.Create, FileAccess.Write, FileShare.None );
						BinSerializer.Serialize( BinStream, TeamList );
						BinStream.Close();
					}
					break;

				case 1 : // Games on Day
					{
						List<Game> GameList = new List<Game>();

						// TODO : Check for running game

						TokenResult = RequestResult["scoreboard"]["gameScore"].Children().ToList();
						foreach( JToken token in TokenResult )
						{
							Game GameData = new Game();
							GameData.QuaterScores = new scores[4];

							JToken GameToken = token.First.First;
							JToken QuatersToken = token.Last.First["quarter"];

							GameData.ID = GameToken["ID"].Value<int>();
							GameData.Date = GameToken["date"].Value<string>();
							GameData.Time = GameToken["time"].Value<string>();
							GameData.Location = GameToken["location"].Value<string>();
							GameData.AwayTeamID = GameToken["awayTeam"]["ID"].Value<int>();
							GameData.HomeTeamID = GameToken["homeTeam"]["ID"].Value<int>();
							for( int i = 0; i < 4; ++i )
							{
								GameData.QuaterScores[i].Quater = i + 1;
								GameData.QuaterScores[i].AwayScore = QuatersToken[i]["awayScore"].Value<int>();
								GameData.QuaterScores[i].HomeScore = QuatersToken[i]["homeScore"].Value<int>();
							}
							GameList.Add( GameData );
						}

						BinaryFormatter BinSerializer = new BinaryFormatter();
						FileStream BinStream = new FileStream( "Data/GameList_" + GameList[0].Date + ".bin", FileMode.Create, FileAccess.Write, FileShare.None );
						BinSerializer.Serialize( BinStream, GameList );
						BinStream.Close();
					}
					break;

				case 2 : // Plays on Day
					{
						TokenResult = RequestResult["gameplaybyplay"]["plays"]["play"].Children().ToList();
						foreach( JToken token in TokenResult )
						{
							Play GamePlay = new Play();

							GamePlay.GameID = PlaysGameList.Last().ID;
							GamePlay.Quater = token["quarter"].Value<int>();
							GamePlay.Time = token["time"].Value<string>();
							if( token["jumpBall"] != null )
							{
								GamePlay.Type = Play.Types.JUMP_BALL;
								GamePlay.SetTarget( token["jumpBall"]["wonBy"].Value<string>() == "HOME" );
							}
							else if( token["fieldGoalAttempt"] != null )
							{
								if( token["fieldGoalAttempt"]["outcome"].Value<string>() == "SCORED" )
									GamePlay.Type = Play.Types.FIELD_GOAL;
								else
									GamePlay.Type = Play.Types.FIELD_GOAL_ATTEMPT;

								GamePlay.SetTarget( IsHomeAbb( "fieldGoalAttempt", token ) );
							}
							else if( token["freeThrowAttempt"] != null )
							{
								if( token["freeThrowAttempt"]["outcome"].Value<string>() != "SCORED" ) continue;

								GamePlay.Type = Play.Types.FREE_THROW_ATTEMPT;
								GamePlay.SetTarget( IsHomeAbb( "freeThrowAttempt" , token ) );
							}
							else if( token["rebound"] != null )
							{
								// check for mysportsfeeds bug
								if( token["rebound"]["offensiveOrDefensive"] == null )
									continue;
								GamePlay.SetTarget( IsHomeAbb( "rebound", token ) );
								GamePlay.Type = ( token["rebound"]["offensiveOrDefensive"].Value<string>() == "DEF" ) ?  Play.Types.DEFENSIVE_REBOUND : Play.Types.OFFENSIVE_REBOUND;
							}
							else if( token["turnover"] != null )
							{
								GamePlay.Type = Play.Types.TURNOVER;
								GamePlay.SetTarget( IsHomeAbb( "turnover", token ) );
							}
							if( GamePlay.Type == Play.Types.UNKNOWN ) continue;

							PlaysList.Add( GamePlay );
						}

						string LastDate = PlaysGameList.Last().Date;
						PlaysGameList.RemoveAt( PlaysGameList.Count - 1 );
						if( PlaysGameList.Count != 0 && LastDate == PlaysGameList.Last().Date ) return;

						BinaryFormatter BinSerializer = new BinaryFormatter();
						FileStream BinStream = new FileStream( "Data/DayPlayList_" + LastDate + ".bin", FileMode.Create, FileAccess.Write, FileShare.None );
						BinSerializer.Serialize( BinStream, PlaysList );
						BinStream.Close();
						PlaysList.Clear();

						if( PlaysGameList.Count == 0 )
						{
							PlaysGameList = null;
							PlaysTeamList = null;
							PlaysList = null;
							HomeAbb = null;
							AwayAbb = null;
						}
					}
					break;
			}

			outputText.AppendText( "Writing data to files completed\n" ); 
		}

		private bool IsHomeAbb( string Cat, JToken token )
		{
			return token[Cat]["teamAbbreviation"].Value<string>() == HomeAbb;
		}

		private void Pull_Click( object sender, EventArgs e )
		{
			if( APIHandle.HasActiveRequest() ) return;

			if( pullType.SelectedIndex == 2 )
			{
				if( PlaysTeamList == null || PlaysGameList == null )
				{
					if( !LoadTeamList() )
					{ 
						outputText.AppendText( "No TeamList.bin found - please do first 'Players and Teams' pull for this season\n" );
						return;
					}
					if( !LoadGameList( dateTimePicker1.Value.ToString( "yyyy-MM-dd" ) ) )
					{
						outputText.AppendText( "No Game_***.bin found for this Date - please do first 'Games on Day' pull for this date and season\n" );
						return;
					}
					
					PlaysList = new List<Play>();
				}
			}
			DisableFields();

			if( APIHandle.StartRequest( GetAPIString() ) )
			{
				if( PlaysList != null && PlaysList.Count > 0 ) return;

				outputText.AppendText( "Start \"" + pullType.Text + "\" Request for " + seasonDropBox.Text );
				if( pullType.SelectedIndex == 1 ) outputText.AppendText( " on " + dateTimePicker1.Text );
				else if( pullType.SelectedIndex == 2 ) outputText.AppendText( " on " + PlaysGameList.Last().Date );
				outputText.AppendText( "\n" );
			}
			else
			{
				ResetFields();
				FullFetch = false;
			}
		}

		private bool LoadTeamList()
		{
			if( !File.Exists( "Data/TeamList.bin" ) ) return false;

			BinaryFormatter BinSerializer = new BinaryFormatter();
			FileStream BinStream = new FileStream( "Data/TeamList.bin", FileMode.Open, FileAccess.Read, FileShare.None );
			PlaysTeamList = (List<Team>)BinSerializer.Deserialize( BinStream );
			BinStream.Close();
			return true;
		}

		private bool LoadGameList( string DateString )
		{
			if( !File.Exists( "Data/GameList_" + DateString + ".bin" ) ) return false;

			BinaryFormatter BinSerializer = new BinaryFormatter();
			FileStream BinStream = new FileStream( "Data/GameList_" + DateString + ".bin", FileMode.Open, FileAccess.Read, FileShare.None );
			PlaysGameList = (List<Game>)BinSerializer.Deserialize( BinStream );
			BinStream.Close();
			return true;
		}

		private void pullAll_Click( object sender, EventArgs e )
		{
			if( !saveData.Checked )
			{
				outputText.AppendText( "'Pull All' only supported with 'Save to file'\n" );
				return;
			}
			if( showJSON.Checked )
			{
				outputText.AppendText( "'Pull All' only supported without 'Show JSON'\n" );
				return;
			}
			if( seasonDropBox.SelectedIndex > 2 )
			{
				outputText.AppendText( "Selected Season not supported\n" );
				return;
			}

			
			if( pullType.SelectedIndex == 0 ) pullType.SelectedIndex = 1;

			if( pullType.SelectedIndex == 1 )
			{
				if( seasonDropBox.SelectedIndex == 0 ) dateTimePicker1.Value = new DateTime( 2015, 10, 27 ); // start of 2015-16 off season
				else if( seasonDropBox.SelectedIndex == 1 ) dateTimePicker1.Value = new DateTime( 2016, 10, 25 ); // start of 2016-17 off season
				else if( seasonDropBox.SelectedIndex == 2 ) dateTimePicker1.Value = new DateTime( 2016, 04, 16 ); // start of 2016-playoff
				FullFetch = true;
			}
			else if( pullType.SelectedIndex == 2 )
			{
				if( !LoadTeamList() )
				{
					outputText.AppendText( "No TeamList.bin found - please do first 'Players and Teams' pull for this season\n" );
					return;
				}
				string[] GameFiles = Directory.GetFiles( Application.StartupPath + "\\Data\\", "GameList_*.bin" );
				if( GameFiles.Length == 0 )
				{
					outputText.AppendText( "No GameList*.bin found - please do first 'Games on Day' pull for this season\n" );
					return;
				}

				List<Game> FullGameList = new List<Game>();
				foreach( string GameFile in GameFiles )
				{
					string Date = GameFile.Substring( GameFile.IndexOf( "GameList_" ) + 9, 10 );
					if( LoadGameList( Date ) ) FullGameList.AddRange( PlaysGameList );
				}
				PlaysGameList = FullGameList;
				PlaysList = new List<Play>();
			}

			Pull_Click( null, null );
		}

		private void ResetFields()
		{
			bt_Pull.Enabled = true;
			bt_Pull.Text = "Pull";

			bt_pullAll.Enabled = true;
			bt_pullAll.Text = "Pull All";

			seasonDropBox.Enabled = true;
			dateTimePicker1.Enabled = true;
		}

		private void DisableFields()
		{
			bt_Pull.Enabled = false;
			bt_Pull.Text = "Waiting";

			bt_pullAll.Enabled = false;
			bt_pullAll.Text = "Waiting";

			seasonDropBox.Enabled = false;
			dateTimePicker1.Enabled = false;
		}

		private string GetAPIString()
		{
			const string APIString = "https://www.mysportsfeeds.com/api/feed/pull/nba/";
			// check for valid generation

			switch( pullType.SelectedIndex )
			{
				case 0 : // Player and Teams
					return APIString + seasonDropBox.Text + "/active_players.json";

				case 1 : // Games on Day
					return APIString + seasonDropBox.Text + "/scoreboard.json?fordate=" + dateTimePicker1.Text;

				case 2 : // Plays on Day
					AwayAbb = PlaysTeamList.Find( p => p.ID == PlaysGameList.Last().AwayTeamID ).Abbreviation;
					HomeAbb = PlaysTeamList.Find( p => p.ID == PlaysGameList.Last().HomeTeamID ).Abbreviation;
					string Date = PlaysGameList.Last().Date.Replace( "-", "" );
					return APIString + seasonDropBox.Text + "/game_playbyplay.json?gameid=" + Date + "-" + AwayAbb + "-" + HomeAbb;
			}

			throw new ArgumentException( "Invalid PullType" );
		}
	}
}
