﻿namespace PullDataBlocks
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.seasonDropBox = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.outputText = new System.Windows.Forms.RichTextBox();
			this.bt_Pull = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.label3 = new System.Windows.Forms.Label();
			this.pullType = new System.Windows.Forms.ComboBox();
			this.showJSON = new System.Windows.Forms.CheckBox();
			this.saveData = new System.Windows.Forms.CheckBox();
			this.bt_pullAll = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// seasonDropBox
			// 
			this.seasonDropBox.DisplayMember = "0";
			this.seasonDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.seasonDropBox.Items.AddRange(new object[] {
            "2015-2016-regular",
            "2016-2017-regular",
            "2016-playoff",
            "current",
            "latest"});
			this.seasonDropBox.Location = new System.Drawing.Point(66, 10);
			this.seasonDropBox.Name = "seasonDropBox";
			this.seasonDropBox.Size = new System.Drawing.Size(121, 21);
			this.seasonDropBox.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(43, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Season";
			// 
			// outputText
			// 
			this.outputText.Location = new System.Drawing.Point(12, 97);
			this.outputText.Name = "outputText";
			this.outputText.ReadOnly = true;
			this.outputText.Size = new System.Drawing.Size(442, 86);
			this.outputText.TabIndex = 2;
			this.outputText.Text = "";
			// 
			// bt_Pull
			// 
			this.bt_Pull.Location = new System.Drawing.Point(375, 8);
			this.bt_Pull.Name = "bt_Pull";
			this.bt_Pull.Size = new System.Drawing.Size(75, 23);
			this.bt_Pull.TabIndex = 3;
			this.bt_Pull.Text = "Pull";
			this.bt_Pull.UseVisualStyleBackColor = true;
			this.bt_Pull.Click += new System.EventHandler(this.Pull_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(205, 13);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(30, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Date";
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePicker1.Location = new System.Drawing.Point(242, 10);
			this.dateTimePicker1.MaxDate = new System.DateTime(2017, 12, 31, 0, 0, 0, 0);
			this.dateTimePicker1.MinDate = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(97, 20);
			this.dateTimePicker1.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(5, 42);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(51, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Pull Type";
			// 
			// pullType
			// 
			this.pullType.DisplayMember = "0";
			this.pullType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.pullType.Items.AddRange(new object[] {
            "Players and Teams",
            "Games on Day",
            "Plays on Day"});
			this.pullType.Location = new System.Drawing.Point(66, 39);
			this.pullType.Name = "pullType";
			this.pullType.Size = new System.Drawing.Size(121, 21);
			this.pullType.TabIndex = 7;
			// 
			// showJSON
			// 
			this.showJSON.AutoSize = true;
			this.showJSON.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.showJSON.Location = new System.Drawing.Point(12, 69);
			this.showJSON.Name = "showJSON";
			this.showJSON.Size = new System.Drawing.Size(84, 17);
			this.showJSON.TabIndex = 8;
			this.showJSON.Text = "Show JSON";
			this.showJSON.UseVisualStyleBackColor = true;
			// 
			// saveData
			// 
			this.saveData.AutoSize = true;
			this.saveData.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.saveData.Checked = true;
			this.saveData.CheckState = System.Windows.Forms.CheckState.Checked;
			this.saveData.Location = new System.Drawing.Point(108, 69);
			this.saveData.Name = "saveData";
			this.saveData.Size = new System.Drawing.Size(79, 17);
			this.saveData.TabIndex = 9;
			this.saveData.Text = "Save to file";
			this.saveData.UseVisualStyleBackColor = true;
			// 
			// bt_pullAll
			// 
			this.bt_pullAll.Location = new System.Drawing.Point(375, 63);
			this.bt_pullAll.Name = "bt_pullAll";
			this.bt_pullAll.Size = new System.Drawing.Size(75, 23);
			this.bt_pullAll.TabIndex = 10;
			this.bt_pullAll.Text = "Pull All";
			this.bt_pullAll.UseVisualStyleBackColor = true;
			this.bt_pullAll.Click += new System.EventHandler(this.pullAll_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(469, 195);
			this.Controls.Add(this.bt_pullAll);
			this.Controls.Add(this.saveData);
			this.Controls.Add(this.showJSON);
			this.Controls.Add(this.pullType);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.dateTimePicker1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.bt_Pull);
			this.Controls.Add(this.outputText);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.seasonDropBox);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox seasonDropBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RichTextBox outputText;
		private System.Windows.Forms.Button bt_Pull;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox pullType;
		private System.Windows.Forms.CheckBox showJSON;
		private System.Windows.Forms.CheckBox saveData;
		private System.Windows.Forms.Button bt_pullAll;
	}
}

