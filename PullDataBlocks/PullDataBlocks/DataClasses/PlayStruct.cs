﻿using System;

namespace DataClasses
{
	[Serializable]
	public class Play
	{
		public enum Types
		{
			UNKNOWN,
			JUMP_BALL,
			FIELD_GOAL,
			FIELD_GOAL_ATTEMPT,
			FREE_THROW_ATTEMPT,
			TURNOVER,
			OFFENSIVE_REBOUND,
			DEFENSIVE_REBOUND,
		};
		public enum Targets
		{
			HOME,
			AWAY
		}
		public int GameID;
		public int Quater;
		public string Time;
		public Types Type = Types.UNKNOWN;
		public Targets Target;
		public void SetTarget( bool Home )
		{
			if( Home ) Target = Targets.HOME;
			else Target = Targets.AWAY;
		}
	}
}
