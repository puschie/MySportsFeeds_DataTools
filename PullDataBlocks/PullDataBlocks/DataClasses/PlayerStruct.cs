using System;

namespace DataClasses
{
	[Serializable]
	public class Player
	{
		public int		ID;
		public string   FirstName;
		public string   LastName;
		public int      JerseyNumber;
		public string   Position;
		public string   Height;
		public int      Weight;
		public string   BirthDate;
		public int      Age;
		public string   BirthCity;
		public string   BirthCountry;
		public bool     IsRookie;
		public int      TeamID;
	}
}