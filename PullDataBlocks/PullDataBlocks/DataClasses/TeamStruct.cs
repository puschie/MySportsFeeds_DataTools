using System;

namespace DataClasses
{
	[Serializable]
	public class Team
	{
		public int		ID;
		public string	City;
		public string	Name;
		public string	Abbreviation;
		public int[]    PlayerIDList;
	}
}