using System;

namespace DataClasses
{
	[Serializable]
	public class Game
	{
		public int		ID;
		public string   Date;
		public string   Time;
		public string   Location;
		public int      AwayTeamID;
		public int      HomeTeamID;
		public scores[] QuaterScores;
	}

	[Serializable]
	public struct scores
	{
		public int  Quater;
		public int  HomeScore;
		public int  AwayScore;
	}
}