﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace PullDataBlocks
{
	class APIRequest
	{
		private const string AuthenticationString = "Basic cHVzY2hpZTpudnIxNDc="; // using my test account
		private string RequestResultData;

		private Thread RequestThread = null;
		private bool ActiveRequest = false;

		private event Action<ResultCodes> ThreadFinished;
		public event Action<ResultCodes> RequestResponse;

		public APIRequest()
		{
			ThreadFinished += RequestCompleted;
		}

		public enum ResultCodes
		{
			UNKNOWN_ERROR = -1,
			NO_ERROR = 0,
			TIME_OUT = 1,
			CONTENT_NOT_FOUND = 204,
			NOT_AUTHENTICATED = 401,
			NOT_AUTHORIZED = 403,
			API_ERROR = 500,
			TRY_AGAIN_LATER = 503
		}

		private async Task<bool> SportsFeedsRequest( string URL )
		{
			HttpWebRequest APIRequest = (HttpWebRequest)WebRequest.Create( new Uri( URL ) );
			APIRequest.Headers.Add( "Authorization", AuthenticationString );

			//APIRequest.AutomaticDecompression = DecompressionMethods.GZip;
			APIRequest.Method = "GET";

			using( WebResponse APIResponse = await APIRequest.GetResponseAsync() )
			{
				using( Stream Response = APIResponse.GetResponseStream() )
				{
					using( StreamReader ResponseReader = new StreamReader( Response ) )
					{
						RequestResultData = ResponseReader.ReadLine();
					}
					return true;
				}
			}
		}

		private void Thread_StartRequest( string URL )
		{
			ResultCodes Result = ResultCodes.NO_ERROR;
			Task<bool> SportsAPIRequest = SportsFeedsRequest( URL );
			try
			{
				if( !SportsAPIRequest.Wait( 3000 ) )
					Result = ResultCodes.TIME_OUT;
			}
			catch( AggregateException e )
			{
				// TODO : handle more errors
				string ErrorMessage = e.InnerException.Message;
				if( ErrorMessage.Contains( "204" ) ) Result = ResultCodes.CONTENT_NOT_FOUND;
				else if( ErrorMessage.Contains( "401" ) ) Result = ResultCodes.NOT_AUTHENTICATED;
				else if( ErrorMessage.Contains( "403" ) ) Result = ResultCodes.NOT_AUTHORIZED;
				else if( ErrorMessage.Contains( "500" ) ) Result = ResultCodes.API_ERROR;
				else if( ErrorMessage.Contains( "503" ) ) Result = ResultCodes.TRY_AGAIN_LATER;
				else Result = ResultCodes.UNKNOWN_ERROR;
			}
			
			ThreadFinished?.Invoke( Result );
		}

		private void RequestCompleted( ResultCodes Success )
		{
			ActiveRequest = false;
			RequestThread = null;

			RequestResponse?.Invoke( Success );
		}

		public bool StartRequest( string URL )
		{
			if( ActiveRequest ) return false;
			Debug.WriteLine( "URL : " + URL );
			RequestThread = new Thread( () => Thread_StartRequest( URL ) );
			RequestThread.Start();

			return true;
		}

		public void AbordRequest()
		{
			ClearThread();
		}

		public bool HasActiveRequest()
		{
			return ActiveRequest;
		}

		public string GetLastResult()
		{
			return RequestResultData;
		}

		private void ClearThread()
		{
			if( RequestThread != null )
			{
				RequestThread.Abort();
				RequestThread = null;
			}
			ActiveRequest = false;
		}
	}
}
