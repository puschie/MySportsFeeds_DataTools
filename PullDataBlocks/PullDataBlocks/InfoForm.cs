﻿using System.Windows.Forms;

namespace PullDataBlocks
{
	public partial class InfoForm : Form
	{
		public InfoForm()
		{
			InitializeComponent();
		}

		public void SetText( string Text )
		{
			richTextBox1.Text = Text;
		}
	}
}
